/**
 * @format
 */

 import { AppRegistry } from 'react-native';
 import App from './App';
 import React from 'react';
 import { Provider } from 'react-redux'
 import { name as appName } from './app.json';
 import { Store } from './src/redux/store';
 
 export default function Main() {
     return (
         <Provider store={Store}>
             <App />
         </Provider>
     )
 }
 
 AppRegistry.registerComponent(appName, () => Main);
 