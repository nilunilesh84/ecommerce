import 'react-native-gesture-handler'
import { LogBox,Text } from 'react-native'
import React, { useEffect } from 'react'
import AppNavigator from './src/navigations'
import { Provider as PaperProvider } from 'react-native-paper';
import PushNotification, { Importance } from 'react-native-push-notification';
import { notificationListner, requestUserPermission } from './src/Notification/Notifications';
import ForegroundHandler from './src/Notification/ForegroundHandler';

LogBox.ignoreLogs([
  "EventEmitter.removeListener('change', ...): Method has been deprecated. Please instead use `remove()` on the subscription returned by `EventEmitter.addListener`",
  "[react-native-gesture-handler] Seems like you\'re using an old API with gesture components, check out new Gestures system!",
]);
const App = () => {
  useEffect(() => {
    PushNotification.createChannel(
      {
        channelId: "channel-id", // (required)
        channelName: "My channel", // (required)
        channelDescription: "A channel to categorise your notifications", // (optional) default: undefined.
        playSound: false, // (optional) default: true
        soundName: "default", // (optional) See `soundName` parameter of `localNotification` function
        importance: Importance.HIGH, // (optional) default: Importance.HIGH. Int value of the Android notification importance
        vibrate: true, // (optional) default: true. Creates the default vibration pattern if true.
      },
      (created) => console.log(`createChannel returned '${created}'`) // (optional) callback returns whether the channel was created, false means it already existed.
    );
    try {
      PushNotification.localNotification({
        /* Android Only Properties */
        channelId: 'channel-id',
        smallIcon: "ic_notification",   
        bigLargeIconUrl: "https://sayas-public.s3.amazonaws.com/1653478095191images(5).jpeg",
        largeIconUrl: "https://sayas-public.s3.amazonaws.com/1653478095191images(5).jpeg",
        vibrate: true, // (optional) default: true
        vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
        title: 'notification', // (optional)
        message: 'notification', // (required)
        body: ' notification', // (required)
        picture: 'https://www.bing.com/th?id=ORMS.4ccef3966920e3b653a35d4b64b77504&pid=Wdp&w=300&h=225&qlt=60&c=1&rs=1&dpr=0.9749999642372131&p=0'
      });
    } catch (error) {
      console.log(error)
    }
    PushNotification.getChannels(function (channel_ids) {
      console.log(channel_ids); // ['channel_id_1'] 
    });
  }, [])
  useEffect(() => {
    requestUserPermission();
    notificationListner();
  }, []);
  return (
    <PaperProvider>
       <AppNavigator />
      <ForegroundHandler />
    </PaperProvider>

  )
}

export default App
