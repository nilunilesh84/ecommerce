import { StyleSheet, Text, View, FlatList } from 'react-native'
import React from 'react'
import CustomButton from '../../components/CustomButton'
import { STYLES } from '../../constants/theme'

const Favoritetopics = () => {
  const data = ['Web', 'Business Analytics', 'RamenJs']
  const data1 = ['HTML', 'CSS', 'Unity']
  const data2 = ['ReactJs', 'NativeJs', 'Javascript']
  const renderItem = ({ item }) => {
    return (
      <View style={{ width: 100, height: 100, backgroundColor: 'red', margin: 12, justifyContent: 'center', alignItems: 'center' }}>
        <Text>{item}</Text>
      </View>
    )
  }
  return (
    <View style={{ ...STYLES, flex: 1 }}>
      <Text>Pick your favourite topics
        Choose your favorite topic to help us deliver the most suitable course for you.
      </Text>
      <FlatList
        data={data}
        renderItem={(item) => renderItem(item)}
        horizontal
        keyExtractor={i => i} />
      <FlatList
        data={data1}
        renderItem={(item) => renderItem(item)}
        horizontal
        keyExtractor={i => i} />
      <FlatList
        data={data2}
        renderItem={(item) => renderItem(item)}
        horizontal
        keyExtractor={i => i} />
      <CustomButton disabled={true} title={' Start your journey'} />
      <Text>You can still change your topic again later</Text>
    </View>
  )
}

export default Favoritetopics

const styles = StyleSheet.create({})