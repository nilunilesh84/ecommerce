import { StyleSheet, Text, View, TextInput, ScrollView } from 'react-native'
import React from 'react'
import { NAVIGATION } from '../../constants/routes'
import { Button } from 'react-native-paper'
import { COLORS } from '../../constants/theme'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import CustomHeader from '../../components/CustomHeader'
import CustomInput from '../../components/CustomInput'
import CustomButton from '../../components/CustomButton'
const Register = ({ navigation }) => {

  return (
    <ScrollView >
      <View style={{ flex: 1, alignItems: 'center', padding: 12 }}>
        <CustomHeader header={"Register"} />
        <Text style={styles.createtext}>Create account ✌</Text>
        <Text>Create your account to start your course lessons</Text>
        <View style={styles.input}>
          <Text>googel</Text>
        </View>
        <Text>Or sign up with your email</Text>
       

        <View style={{ paddingHorizontal: 32, alignItems: 'center' }}>
          <Text>By continuing you agree to our <Text style={{ color: COLORS.primary }}>Terms &
            Conditions <Text style={{ color: 'gray' }}>and</Text> Privacy Policy</Text></Text>
        </View>
        <Text style={{ color: COLORS.primary, marginTop: 41 }}>I already have an account</Text>
        {/* <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Button onPress={() => navigation.navigate(NAVIGATION.LOGIN)}>
          GO to Login Screen
        </Button>
      </View> */}
      </View>
    </ScrollView>
  )
}

export default Register

const styles = StyleSheet.create({
  input: {
    backgroundColor: "#FFFF",
    flexDirection: 'row',
    alignItems: 'center',
    width: '90%',
    borderRadius: 28,
    paddingHorizontal: 22,
    height: 55,
    marginVertical: 8,
  }, createtext: {
    fontSize: 22,
    color: COLORS.black,
    alignSelf: 'flex-start'
  },
  button2: {
    width: '89%',
    borderColor: 'black',
    borderRadius: 20,
    marginVertical: 11,
    borderWidth: 1,
    backgroundColor: COLORS.black,
    color: 'white'
  }
})