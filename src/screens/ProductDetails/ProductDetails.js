import { BackHandler, Image, ScrollView, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { FONTS, SIZES, STYLES } from '../../constants/theme'
import { NAVIGATION } from '../../constants/routes'
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getSingleProduct } from '../../redux/actions';
import CustomLoader from '../../components/CustomLoader';
import { addToCart } from '../../redux/actions/cart.action';
import { Badge } from 'react-native-paper';

const ProductDetails = ({ navigation, route }) => {
    const ids = route.params?.id
    const { loaderShow } = useSelector(state => state.loaderReducer)
    const { singleProduct } = useSelector(state => state.userReducer)
    const { cart } = useSelector(state => state.cartReducers)
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(getSingleProduct(ids))
    }, [])
    BackHandler.addEventListener('hardwareBackPress', () => {
        navigation.goBack()
        return true;
    }, []);
    return (
        <>
            {loaderShow ? <CustomLoader shown={loaderShow} /> :
                <View style={{ flex: 1, justifyContent: 'space-between', display: 'flex' }}>
                    <View style={{ flexDirection: 'row', width: SIZES.width, justifyContent: 'space-between', backgroundColor: 'lightblue', alignItems: 'center', paddingHorizontal: 8 }}>
                        <View style={{ height: SIZES.base, ...styles.left }}>
                            <AntDesign onPress={() => navigation.goBack()} name='left' size={20} />
                        </View>
                        <View>
                            {
                                cart.length ? <Badge style={{ position: 'absolute', top: -12 }}>{cart.length}</Badge> : null
                            }
                            <Ionicons onPress={() => navigation.navigate(NAVIGATION.CART)} name='cart-outline' size={SIZES.h3} color={'black'} />
                        </View>
                    </View>
                    <ScrollView contentContainerStyle={{ alignItems: 'center', padding: SIZES.base }}>
                        <Image resizeMode='contain' style={{ height: SIZES.height * .7, width: '90%' }} source={{ uri: singleProduct.image }} />
                        <Text style={FONTS.h4}>{singleProduct.title}</Text>
                        <Text style={FONTS.h4}>{singleProduct.price}</Text>
                        <Text style={FONTS.h4}>{singleProduct.category}</Text>
                        <Text style={FONTS.h4}>{singleProduct.description}</Text>
                    </ScrollView>
                    <View style={{ width: SIZES.width, flexDirection: 'row' }}>
                        <View style={{ width: SIZES.width / 2, ...STYLES, height: SIZES.largeTitle * .9, elevation: 12, backgroundColor: 'white' }}>
                            {
                                cart.filter(ite => ite.id === singleProduct.id)[0] ?
                                    <Text onPress={() => navigation.navigate(NAVIGATION.CART)}>GO TO CART</Text> :
                                    <Text onPress={() => dispatch(addToCart(singleProduct))}>ADD TO CART</Text>
                            }
                        </View>
                        <View style={{ width: SIZES.width / 2, backgroundColor: 'orange', ...STYLES, height: SIZES.largeTitle * .9, elevation: 12 }}>
                            <Text onPress={() => alert('BUY NOW')}>BUY NOW</Text>
                        </View>
                    </View>
                </View>
            }
        </>
    )
}

export default ProductDetails

const styles = StyleSheet.create({
    left: {
        width: 30,
        height: 30,
        backgroundColor: 'white',
        borderRadius: 12,
        justifyContent: 'center',
        alignItems: 'center',
        // margin: 12,
    },
})