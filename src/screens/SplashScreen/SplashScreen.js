import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { COLORS, SIZES, STYLES } from '../../constants/theme'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { useDispatch, } from 'react-redux'
import { userLogin } from '../../redux/actions/userActions';
const SplashScreen = ({ navigation }) => {
  const dispatch = useDispatch()
  setTimeout(async () => {
    const user = await AsyncStorage.getItem('setUser')
    if (!user) {
      navigation.replace('AuthNavigator')
    } else {
      dispatch(userLogin(JSON.parse(user)))
      navigation.replace('HomeNavigator')
    }
  }, 2000);
  return (
    <Image resizeMode='contain' source={require('../../assets/yash.jpg')} style={{flex:1, width: SIZES.width }} />
  )
}

export default SplashScreen
