import { StyleSheet, Text, View, TextInput, ScrollView, Pressable } from 'react-native'
import React, { useState } from 'react'
import { NAVIGATION } from '../../constants/routes'
import { Formik } from 'formik';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { COLORS } from '../../constants/theme'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Feather from 'react-native-vector-icons/Feather'
import CustomButton from '../../components/CustomButton'
import { useDispatch } from 'react-redux';
import { userLogin } from '../../redux/actions/userActions';
const Login = ({ navigation }) => {
  const dispatch = useDispatch()
  const [showpassword, setshowpassword] = useState(true)
  return (
    <ScrollView keyboardShouldPersistTaps='always' >
      <View style={{ flex: 1, alignItems: 'center', padding: 12, marginTop: 50 }}>
        <Text style={styles.createtext}>Welcome back 👏</Text>
        <Text>Create your account to start your course lessons</Text>
        <View style={styles.input}>
          <Text>googel</Text>
        </View>
        <Text style={{ marginVertical: 20 }}>Or Login with your email</Text>
        <Formik
          initialValues={{ email: '', password: '' }}
          onSubmit={values => console.log(values)}
        >
          {({ handleChange, handleBlur, handleSubmit, values, errors, isValid }) => (
            <View style={{ width: '100%', alignItems: 'center' }}>
              {/* <TextInput
              onChangeText={handleChange('email')}
              onBlur={handleBlur('email')}
              placeholder='enter'
              value={values.email}
            /> */}
              <View style={{ ...styles.input, borderColor: values.email ? COLORS.primary : 'white' }}>
                <Feather name='mail' size={21} style={{ marginRight: 20 }} />
                <TextInput
                  onChangeText={handleChange('email')}
                  placeholder='Your email'
                  style={{ width: '90%' }} />
              </View>
              <View style={{ ...styles.input, borderColor: values.password ? COLORS.primary : 'white' }}>
                <MaterialCommunityIcons style={{ marginRight: 20 }} name='lock-outline' size={21} />
                <TextInput onChangeText={handleChange('password')} secureTextEntry={showpassword} placeholder='Your password' style={{ width: '90%' }} />
                <FontAwesome5 onPress={() => setshowpassword(!showpassword)} style={styles.searchIcon} name={showpassword ? "eye" : "eye-slash"} color='#5A5A5A' />
              </View>
              <Text onPress={() => navigation.navigate(NAVIGATION.RESET)} style={{ color: COLORS.primary, marginVertical: 20, alignSelf: 'flex-end' }}>Forgot Password ?</Text>
              <CustomButton
                disabled={Boolean(values.email && values.password)}
                btnstyle={{ backgroundColor: COLORS.black }}
                onPress={async () => {
                  navigation.replace(NAVIGATION.HOME_N)
                  dispatch(userLogin(values.email))
                  await AsyncStorage.setItem('setUser', JSON.stringify(values.email))
                }}
                title={'Sign In'} />
              <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 20 }}>
                <Text>Don't have an account yet ? </Text>
                <Text style={{color: COLORS.primary, }} onPress={() => navigation.navigate(NAVIGATION.REGISTER)} >SIGN UP</Text>
              </View>
            </View>
          )}
        </Formik>
      </View>
    </ScrollView>
  )
}


const styles = StyleSheet.create({
  input: {
    backgroundColor: "#FFFF",
    flexDirection: 'row',
    alignItems: 'center',
    width: '90%',
    borderRadius: 20,
    paddingHorizontal: 22,
    height: 55,
    marginVertical: 8,
    borderWidth: 1
  }, createtext: {
    fontSize: 22,
    color: COLORS.black,
    alignSelf: 'flex-start'
  },
  button2: {
    width: '89%',
    borderColor: 'black',
    borderRadius: 20,
    marginVertical: 11,
    borderWidth: 1,
    backgroundColor: COLORS.black,
    color: 'white'
  },
  searchIcon: {
    fontSize: 17,
    right: 20
  },
})

export default Login