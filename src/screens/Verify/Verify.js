import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import CustomHeader from '../../components/CustomHeader'
import CustomButton from '../../components/CustomButton'
import { COLORS, FONTS, STYLES } from '../../constants/theme'
import { NAVIGATION } from '../../constants/routes'

const Verify = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <CustomHeader header={'Verify'}
                leftshow={true}
                onPressLeft={() => navigation.goBack()}
            />
            <View style={{marginVertical:52}}>
                <Text style={styles.createtext}>Verify Mobile 📲</Text>
                <Text>Ceasdfljaksdflasdflaskdfalsdfaksda</Text>
                <Text>Ceasdfljaksdflasdflaskdfalsdfaksda</Text>
            </View>
            <View style={{marginVertical:52}}>
            </View>
            <CustomButton 
                title={'Verify'}
                btnstyle={{marginVertical:53}}
                onPress={() =>navigation.navigate(NAVIGATION.LOGIN)} />
            <Text>Not retrieve any code? <Text style={{ ...FONTS.h5, color: COLORS.primary }}>Resend code</Text></Text>
        </View>
    )
}

export default Verify

const styles = StyleSheet.create({
    container: {
        ...STYLES,
    }, createtext: {
        fontSize: 22,
        color: COLORS.black,
        alignSelf: 'flex-start'
    },
})