import React from 'react';

import { View, StyleSheet, Text, Platform, Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window')
// const { width, height } = Dimensions.get('screen')
export default function Settings() {

  return (
    <View style={styleSheet.MainContainer}>

      <Text style={{ fontSize: 28, color: 'black', textAlign: 'center' }}> Create View in Percentage in React Native </Text>

      <View style={{
        backgroundColor: '#00C853',
        width: '80%',
        height: '40%',
        justifyContent: 'center',
        margin: 10,
      }}>

        <Text style={styleSheet.text}> View - 1 View - 1View - </Text>
        <Text style={styleSheet.text}>View1View1Vie1View</Text>

      </View>

      <View style={{
        backgroundColor: '#AA00FF',
        width: '80%',
        height: '40%',
        justifyContent: 'center',
      }}>

        <Text style={{ ...styleSheet.text, fontSize: width * .08 }}>ViewView2ViewVie</Text>
        <Text>{width}</Text>
      </View>

    </View>
  );
}

const styleSheet = StyleSheet.create({

  MainContainer: {
    flex: 1,
    alignItems: 'center',
    paddingTop: (Platform.OS === 'ios') ? 20 : 0
  },

  text: {
    fontSize: 26,
    color: 'white',
    textAlign: 'center',

  },

});