import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import CustomButton from '../../components/CustomButton'
import RNRestart from 'react-native-restart'; 
import { COLORS, SIZES, STYLES } from '../../constants/theme'
import { NAVIGATION } from '../../constants/routes'
const UserQuestion = ({ navigation }) => {
  const [pages, setpages] = useState('1')
  const [option, setoption] = useState(false)
  const [optionvalue, setoptionvalue] = useState('')
  const [optionvalue2, setoptionvalue2] = useState('')
  const [optionvalue3, setoptionvalue3] = useState('')
  const [optionvalue4, setoptionvalue4] = useState('')
  useEffect(() => {
    if (pages === '1') {
      if (option) {
        setoptionvalue('Theory')
      } else {
        setoptionvalue('Real-life')
      }
    } else if (pages === '2') {
      if (option) {
        setoptionvalue2('Learning Code')
      } else {
        setoptionvalue2('Writing Code')
      }
    } else if (pages === '3') {
      if (option) {
        setoptionvalue3('Offline')
      } else {
        setoptionvalue3('Online')
      }
    } else if (pages === '4') {
      if (option) {
        setoptionvalue4('Daily')
      } else {
        setoptionvalue4('Rarely')
      }
    }
  }, [option, pages, optionvalue, optionvalue2, optionvalue3, optionvalue4])
  return (
    <View style={{ ...STYLES, justifyContent: 'space-between', padding: 18, flex: 1 }}>
      {
        !pages ?
          <>
            <View>
              <View style={{ width: 200, height: 200, backgroundColor: 'red' }}></View>
              <Text>UserQuestionUserQuestionUser
                QuestionUserQuestionUserQuestion</Text>
            </View>
            <View >
              <Text>Find out which careers, languages,
                and courses suit your personal
                interests and strengths best.
                Start</Text>
            </View>
            <CustomButton btnstyle={{ backgroundColor: COLORS.primary }} title={'Start'} disabled onPress={() => setpages('1')} />
          </> :
          pages == '1' ?
            <View style={{ width: SIZES.width, justifyContent: 'space-between', height: SIZES.height, padding: 12, flex: 1 }}>
              <Text style={{ alignSelf: 'center', color: COLORS.primary }}>1/8</Text>
              <Text style={{ alignSelf: 'center' }}>
                I prefer to spend
                time thinking about...
              </Text>
              <View style={{ width: '100%', alignItems: 'center' }}>
                <CustomButton titleStyle={{ color: !option ? 'black' : 'white' }} btnstyle={{ backgroundColor: option ? 'black' : 'white', height: 100, marginVertical: 5 }} title={'Theory'} disabled onPress={() => setoption(!option)} />
                <CustomButton titleStyle={{ color: option ? 'black' : 'white' }} btnstyle={{ backgroundColor: !option ? 'black' : 'white', height: 100, marginVertical: 5 }} title={'Real-life'} disabled 
                onPress={() => {
                  RNRestart.Restart()
                  // setoption(!option)
                }
                } />
              </View>
              <View style={{ width: '100%', alignItems: 'center', }}>
                <CustomButton btnstyle={{ backgroundColor: COLORS.primary }} title={'Next'} disabled onPress={() => setpages('2')} />
                <CustomButton btnstyle={{ backgroundColor: COLORS.primary }} title={'Skip'} disabled onPress={() => navigation.navigate(NAVIGATION.FAVORITE_TOPICS)} />
              </View>
            </View> : pages == '2' ?
              <View style={{ width: SIZES.width, justifyContent: 'space-between', height: SIZES.height, padding: 12, flex: 1 }}>
                <Text style={{ alignSelf: 'center', color: COLORS.primary }}>2/8</Text>
                <Text style={{ alignSelf: 'center' }}>
                  I prefer to learn
                  more of
                  ...
                </Text>
                <View style={{ width: '100%', alignItems: 'center' }}>
                  <CustomButton titleStyle={{ color: !option ? 'black' : 'white' }} btnstyle={{ backgroundColor: option ? 'black' : 'white', height: 100, marginVertical: 5 }} title={'Learning Code'} disabled onPress={() => setoption(!option)} />
                  <CustomButton titleStyle={{ color: option ? 'black' : 'white' }} btnstyle={{ backgroundColor: !option ? 'black' : 'white', height: 100, marginVertical: 5 }} title={'Writing Code'} disabled onPress={() => setoption(!option)} />
                </View>
                <View style={{ width: '100%', alignItems: 'center', }}>
                  <CustomButton btnstyle={{ backgroundColor: COLORS.primary }} title={'Next'} disabled onPress={() => setpages('3')} />
                  <CustomButton btnstyle={{ backgroundColor: COLORS.primary }} title={'Skip'} disabled onPress={() => navigation.navigate(NAVIGATION.FAVORITE_TOPICS)} />
                </View>
              </View> : pages == '3' ?
                <View style={{ width: SIZES.width, justifyContent: 'space-between', height: SIZES.height, padding: 12, flex: 1 }}>
                  <Text style={{ alignSelf: 'center', color: COLORS.primary }}>3/8</Text>
                  <Text style={{ alignSelf: 'center' }}>
                    I usually feel comfortable
                    to learn ...
                  </Text>
                  <View style={{ width: '100%', alignItems: 'center' }}>
                    <CustomButton titleStyle={{ color: !option ? 'black' : 'white' }} btnstyle={{ backgroundColor: option ? 'black' : 'white', height: 100, marginVertical: 5 }} title={'Offline'} disabled onPress={() => setoption(!option)} />
                    <CustomButton titleStyle={{ color: option ? 'black' : 'white' }} btnstyle={{ backgroundColor: !option ? 'black' : 'white', height: 100, marginVertical: 5 }} title={'Online'} disabled onPress={() => setoption(!option)} />
                  </View>
                  <View style={{ width: '100%', alignItems: 'center', }}>
                    <CustomButton btnstyle={{ backgroundColor: COLORS.primary }} title={'Next'} disabled onPress={() => setpages('4')} />
                    <CustomButton btnstyle={{ backgroundColor: COLORS.primary }} title={'Skip'} disabled onPress={() => navigation.navigate(NAVIGATION.FAVORITE_TOPICS)} />
                  </View>
                </View> :
                <View style={{ width: SIZES.width, justifyContent: 'space-between', height: SIZES.height, padding: 12, flex: 1 }}>
                  <Text style={{ alignSelf: 'center', color: COLORS.primary }}>4/8</Text>
                  <Text style={{ alignSelf: 'center' }}>
                    I try to be up to date
                    with latest technologies...
                  </Text>
                  <View style={{ width: '100%', alignItems: 'center' }}>
                    <CustomButton titleStyle={{ color: !option ? 'black' : 'white' }} btnstyle={{ backgroundColor: option ? 'black' : 'white', height: 100, marginVertical: 5 }} title={'Daily'} disabled onPress={() => setoption(!option)} />
                    <CustomButton titleStyle={{ color: option ? 'black' : 'white' }} btnstyle={{ backgroundColor: !option ? 'black' : 'white', height: 100, marginVertical: 5 }} title={'Rarely'} disabled onPress={() => setoption(!option)} />
                  </View>
                  <View style={{ width: '100%', alignItems: 'center', }}>
                    <CustomButton btnstyle={{ backgroundColor: COLORS.primary }} title={'Next'} disabled onPress={() => navigation.navigate(NAVIGATION.FAVORITE_TOPICS)} />
                    <CustomButton btnstyle={{ backgroundColor: COLORS.primary }} title={'Skip'} disabled onPress={() => navigation.navigate(NAVIGATION.FAVORITE_TOPICS)} />
                  </View>
                </View>
      }

    </View>
  )
}

export default UserQuestion

const styles = StyleSheet.create({})