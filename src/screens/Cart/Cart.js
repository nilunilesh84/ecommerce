import { BackHandler, FlatList, Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { SIZES, STYLES } from '../../constants/theme';
import Imae from '../../assets/1024-.png'
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useDispatch, useSelector } from 'react-redux';
import { changeqtyofProduct, removeToCart } from '../../redux/actions/cart.action';
const MyCart = ({ navigation }) => {

  const { cart } = useSelector(state => state.cartReducers)
  BackHandler.addEventListener('hardwareBackPress', () => {
    navigation.goBack()
    return true;
  }, []);
  const dispatch = useDispatch()
  const changeQty = (data) => {
    dispatch(changeqtyofProduct({ id: data.id, qty: data.qty }))
  }
  const reducecart = cart.reduce((acc, curr) => acc + curr.price * curr.qty, 0)
  const renderItem = ({ item }) => {
    return (
      <View style={{ margin: 6, height: 200, elevation: 12, backgroundColor: 'white', ...STYLES }}>
        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
          <View style={{ width: '30%', alignItems: 'center' }}>
            <Image resizeMode='contain' style={{ width: '100%', height: '60%', }} source={{ uri: item.image }} />
            <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', justifyContent: 'space-around', top: 12 }}>
              <AntDesign onPress={() => {
                changeQty({ id: item.id, qty: item.qty + 1 })
              }} name='pluscircleo' size={SIZES.h3} color={'black'} />
              <Text>{item.qty}</Text>
              <AntDesign onPress={() => changeQty({ id: item.id, qty: item.qty === 1 ? 1 : item.qty - 1 })} name='minuscircleo' size={SIZES.h3} color={'black'} />
            </View>
          </View>
          <View style={{ width: '60%' }}>
            <Text>{item.title}</Text>
            <Text>{item.price}</Text>
          </View>
        </View>
        <View style={{ width: SIZES.width, flexDirection: 'row' }}>
          <View style={{ width: SIZES.width / 2, ...STYLES, height: SIZES.largeTitle * .9 }}>
            <Text onPress={() => console.log('first')}>Save for later</Text>
          </View>
          <View style={{ width: SIZES.width / 2, backgroundColor: 'orange', ...STYLES, height: SIZES.largeTitle * .9 }}>
            <Text onPress={() => dispatch(removeToCart(item.id))}>Remove</Text>
          </View>
        </View>
      </View>
    )
  }
  return (
    <View style={styles.container}>
      <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: SIZES.base }}>
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <Ionicons onPress={() => navigation.openDrawer()} name='menu' size={SIZES.h1} color={'black'} />
          <Image resizeMode='contain' style={{ width: SIZES.mediumTitle, height: SIZES.mediumTitle, }} source={Imae} />
        </View>
      </View>
      {
        cart.length ?
          <FlatList
            data={cart}
            renderItem={renderItem}
            keyExtractor={(item, index) => index}
          /> : <Text>Cart is empty</Text>
      }
      <View style={{ flexDirection: 'row', alignItems: 'center', height: 40, backgroundColor: '#fff', justifyContent: 'space-between' }}>
        <View>
          <Text>Total</Text>
          <Text>$ {Math.round(reducecart)}</Text>
        </View>
        <View style={{ width: SIZES.width / 2, backgroundColor: 'orange', ...STYLES, height: SIZES.largeTitle * .9, elevation: 12 }}>
          <Text onPress={() => alert('BUY NOW')}>BUY NOW</Text>
        </View>
      </View>
    </View>
  )
}

export default MyCart

const styles = StyleSheet.create({
  container: { flex: 1 },
  textWrapper: {
    backgroundColor: 'red', ...STYLES,
    height: '30%', // 70% of height device screen
    width: '90%'   // 80% of width device screen
  },
  textWrapper1: {
    backgroundColor: 'blue', ...STYLES,
    height: '30%', // 70% of height device screen
    width: '95%'  // 80% of width device screen
  },
  textWrapper2: {
    backgroundColor: 'pink', ...STYLES,
    height: '30%', // 70% of height device screen
    width: '100%'   // 80% of width device screen
  },
  myText: {
    fontSize: 25// End result looks like the provided UI mockup
  }
});