import { Text, View, FlatList, Image, Pressable, BackHandler } from 'react-native'
import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { NAVIGATION } from '../../constants/routes'
import Imae from '../../assets/1024-.png'
import Ionicons from 'react-native-vector-icons/Ionicons';
import CustomLoader from '../../components/CustomLoader'

import { Badge } from 'react-native-paper';
import { changeScrollpostion, getProducts, } from '../../redux/actions'
import { FONTS, SIZES } from '../../constants/theme'
const Home = ({ navigation }) => {

  const { loaderShow } = useSelector(state => state.loaderReducer)
  const { products, user, scrollPosition } = useSelector(state => state.userReducer)

  const { cart } = useSelector(state => state.cartReducers)
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(getProducts())
  }, [])
  const handleScroll = (event) => {
    let yOffset = event.nativeEvent.contentOffset.y / SIZES.height * .35;
    dispatch(changeScrollpostion(yOffset))
  }
  const renderItem = ({ item }) => {
    return (
      <Pressable onPress={() => {
        navigation.navigate(NAVIGATION.PRODUCTDETAIL, { id: item.id })
      }} style={{ height: SIZES.height * .35, borderRadius: SIZES.base1, width: '46%', backgroundColor: 'white', margin: SIZES.base1, elevation: 12, padding: SIZES.base }}>
        <Image resizeMode='contain' style={{ height: SIZES.height * .3 }} source={{ uri: item.image }} />
        <Text style={SIZES.h2}>{item.title.substring(0, 15)}</Text>
      </Pressable>
    )
  }
  BackHandler.addEventListener('hardwareBackPress', () => {
    BackHandler.exitApp()
    return true;
  }, []);
  return (
    <View style={{ flex: 1 }}>
      <CustomLoader shown={loaderShow} />
      <View style={{ flex: 1 }}>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal: SIZES.base, height: 50 }}>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Ionicons onPress={() => navigation.openDrawer()} name='menu' size={SIZES.h1} color={'black'} />
            <Image resizeMode='contain' style={{ width: SIZES.mediumTitle, height: SIZES.mediumTitle, }} source={Imae} />
          </View>
          <View>
            {
              cart.length ? <Badge style={{ position: 'absolute', top: -12 }}>{cart.length}</Badge> : null
            }
            <Ionicons onPress={() => navigation.navigate(NAVIGATION.CART)} name='cart-outline' size={SIZES.h3} color={'black'} />
          </View>
        </View>
        <Text style={{ margin: SIZES.base, ...FONTS.h4 }}>Welcome {user}</Text>
        <FlatList
          horizontal={false}
          data={products}
          initialScrollIndex={scrollPosition}
          onScroll={(event) => handleScroll(event)}
          // columnWrapperStyle={{ flexWrap: 'wrap', alignSelf: 'center' }}
          numColumns={2}
          renderItem={renderItem}
          keyExtractor={(item, index) => index} />
      </View>
    </View>
  )
}

export default Home

