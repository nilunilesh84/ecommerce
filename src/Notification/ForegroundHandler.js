import { useEffect } from "react";
import messaging from '@react-native-firebase/messaging';
import PushNotification from "react-native-push-notification";

const ForegroundHandler = () => {
    useEffect(() => {
        const unsubscribe = messaging().onMessage((remoteMessage) => {
            console.log('handle in foreground', remoteMessage)
            const { notification, messageId } = remoteMessage
            PushNotification.localNotification({
                channelId: 'channel-id',
                // id: messageId,
                color: 'green', 
                body: notification.body,
                title: notification.title,
                vibrate: true,
                playSound: true,
                message: notification.body
            })
        })
        return unsubscribe
    }, [])
    return null
}
export default ForegroundHandler