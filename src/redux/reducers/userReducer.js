import { SET_USER, LOG_OUT, SET_PRODUCTS, SET_SCROLL, SINGLE_PRODUCT } from "../actions/userActions"
const initState = {
    user: null,
    isLogin: false,
    products: [],
    scrollPosition: 0,
    singleProduct: []
}


export default function userReducer(state = initState, action) {
    const { payload, type } = action
    switch (type) {
        case SET_USER: return { ...state, user: payload, isLogin: true }
        case LOG_OUT: return { ...state, user: null, isLogin: false }
        case SET_PRODUCTS: return { ...state, products: payload }
        case SET_SCROLL: return { ...state, scrollPosition: payload }
        case SINGLE_PRODUCT: return { ...state, singleProduct: payload }
        default: return state
    }
}