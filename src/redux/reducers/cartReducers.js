import { ADD_TO_CART, REMOVE_FROM_CART, CHANGE_QTY } from "../actions/cart.action"
const initState = {
    cart: [],
}


export default function cartReducers(state = initState, action) {
    const { payload, type } = action
    switch (type) {
        case ADD_TO_CART: return { ...state, cart: [...state.cart, payload] }
        case REMOVE_FROM_CART: return { ...state, cart: state.cart.filter(ite => ite.id !== payload) }
        case CHANGE_QTY: return { ...state, cart: state.cart.filter(ite => ite.id === payload.id ? ite.qty = payload.qty : ite.qty) }
        default: return state
    }
}