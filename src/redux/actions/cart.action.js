
export const ADD_TO_CART = "ADD_TO_CART"
export const REMOVE_FROM_CART = "REMOVE_FROM_CART"
export const CHANGE_QTY = "CHANGE_QTY"


export const addToCart = (data) => async dispatch => {
    dispatch({ type: ADD_TO_CART, payload: { ...data, qty: 1 } })
}
export const removeToCart = (data) => async dispatch => {
    dispatch({ type: REMOVE_FROM_CART, payload: data })
}
export const changeqtyofProduct = (data) => async dispatch => {
    dispatch({ type: CHANGE_QTY, payload: data })
}