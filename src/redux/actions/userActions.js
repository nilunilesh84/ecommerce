import AsyncStorage from "@react-native-async-storage/async-storage"
import axios from "axios"

export const SET_USER = "SET_USER"
export const LOG_OUT = "LOG_OUT"
export const SET_PRODUCTS = "SET_PRODUCTS"
export const SINGLE_PRODUCT = "SINGLE_PRODUCT"
export const SET_SCROLL = "SET_SCROLL"

const BASE_URL = 'https://fakestoreapi.com/products'


export const userLogin = (email) => async dispatch => {
    dispatch({ type: SET_USER, payload: email })
}
export const logout = () => async dispatch => {
    dispatch({ type: LOG_OUT })
    await AsyncStorage.clear()
}
export const getProducts = () => async dispatch => {
    try {
        const { data } = await axios.get(BASE_URL)
        dispatch({ type: SET_PRODUCTS, payload: data })
    } catch (error) {
        console.log(error)
    }
}
export const getSingleProduct = (id) => async dispatch => {
    try {
        const { data } = await axios.get(BASE_URL + `/${id}`)
        dispatch({ type: SINGLE_PRODUCT, payload: data })
    } catch (error) {
        console.log(error.response)
    }
}
export const changeScrollpostion = (data) => async dispatch => {
    dispatch({ type: SET_SCROLL, payload: data })
}