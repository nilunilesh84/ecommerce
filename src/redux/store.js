import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import userReducer from './reducers/userReducer'
import loaderReducer from './reducers/loaderReducer'
import cartReducers from './reducers/cartReducers'


const rootReducer = combineReducers({
    userReducer,
    loaderReducer,
    cartReducers
})

export const Store = createStore(rootReducer, applyMiddleware(thunk))
