import React from 'react'
import { StyleSheet} from 'react-native'
import Profile from '../screens/Profile'
import Settings from '../screens/Settings'
import Home from '../screens/Home'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { NAVIGATION } from '../constants/routes'
import CustomDrawer from '../components/CustomDrawer'
import { createDrawerNavigator } from '@react-navigation/drawer';
import MyCart from '../screens/Cart/Cart'
const DrawerNavigator = () => {

  const Drawer = createDrawerNavigator();
  return (
    <Drawer.Navigator drawerContent={props => <CustomDrawer {...props} />} screenOptions={{ headerShown: false, }} >
      <Drawer.Screen options={{
        drawerLabel: 'Home',
        drawerIcon: ({ color }) => <Ionicons name='home-outline' size={22} color={color} />
      }} name={NAVIGATION.HOME} component={Home} />
      <Drawer.Screen options={{
        drawerLabel: 'Profile',
        drawerIcon: ({ color }) => <Ionicons name='md-person-outline' size={22} color={color} />
      }} name={NAVIGATION.PROFILE} component={Profile} />
      <Drawer.Screen options={{
        drawerLabel: 'Settings',
        drawerIcon: ({ color }) => <Ionicons name='settings-outline' size={22} color={color} />
      }} name={NAVIGATION.SETTINGS} component={Settings} />
      <Drawer.Screen options={{
        drawerLabel: 'My Cart',
        drawerIcon: ({ color }) => <Ionicons name='cart-outline' size={22} color={color} />
      }} name={NAVIGATION.CART} component={MyCart} />
    </Drawer.Navigator>
  )
}

export default DrawerNavigator

const styles = StyleSheet.create({})