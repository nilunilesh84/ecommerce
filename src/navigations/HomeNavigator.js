import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import DrawerNavigator from './DrawerNavigator';
import UserQuestion from '../screens/UserQuestion'
import Favoritetopics from '../screens/FavoriteTopics';
import { NAVIGATION } from '../constants/routes';
import ProductDetails from '../screens/ProductDetails';

const HomeNavigator = () => {


  const Stack = createNativeStackNavigator();
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }} >
      <Stack.Screen name={NAVIGATION.DRAWER} component={DrawerNavigator} />
      <Stack.Screen name={NAVIGATION.USER_QUES} component={UserQuestion} />
      <Stack.Screen name={NAVIGATION.FAVORITE_TOPICS} component={Favoritetopics} />
      <Stack.Screen name={NAVIGATION.PRODUCTDETAIL} component={ProductDetails} />
    </Stack.Navigator>
  )
}

export default HomeNavigator
