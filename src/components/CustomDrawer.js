import { Text, View, Pressable } from 'react-native';
import React from 'react';
import {
    DrawerContentScrollView,
    DrawerItemList,
} from '@react-navigation/drawer';
import { logout } from '../redux/actions/userActions'
import { Avatar } from 'react-native-paper';
import { useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { NAVIGATION } from '../constants/routes';
const CustomDrawer = props => {
    const dispatch = useDispatch()
    const navigation = useNavigation()
    return (
        <View style={{ flex: 1 }}>
            <DrawerContentScrollView  {...props}>
                <View style={{ height: 200, backgroundColor: 'red', justifyContent: 'center', padding: 12 }}>
                    <Pressable onPress={() => alert('working on progress')}>
                        <Avatar.Icon icon="account" style={{ backgroundColor: 'purple' }} size={100} />
                    </Pressable>
                </View>
                <DrawerItemList {...props} />
            </DrawerContentScrollView>
            <View style={{ alignItems: 'center', bottom: 15 }}>
                <Text onPress={() => {
                    dispatch(logout())
                    navigation.replace(NAVIGATION.AUTH)
                }}>Logout</Text>
                {/* <Text>Version </Text> */}
            </View>
        </View>
    );
};

export default CustomDrawer;

