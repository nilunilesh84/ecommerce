import { StyleSheet, Text, View, TextInput } from 'react-native'
import React from 'react'

import FontAwesome from 'react-native-vector-icons/FontAwesome'
const CustomInput = ({placeholder }) => {
    return (
        <View style={styles.input}>
            <FontAwesome name='user' size={21} style={{marginRight:20}} />
            <TextInput placeholder={placeholder} />
        </View>
    )
}

export default CustomInput

const styles = StyleSheet.create({
    input: {
        backgroundColor: "#FFFF",
        flexDirection: 'row',
        alignItems: 'center',
        width: '90%',
        borderRadius: 28,
        paddingHorizontal: 22,
        marginVertical:8,
        height:55
    }
})