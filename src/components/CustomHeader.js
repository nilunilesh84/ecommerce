import { StyleSheet, Text, View, Pressable, Image } from 'react-native'
import React from 'react'
import AntDesign from 'react-native-vector-icons/AntDesign'
import { FONTS, SIZES, STYLES } from '../constants/theme'

const CustomHeader = ({ leftshow, rightshow, header, onPressLeft, onPressRight, source, imgstyle, }) => {
  return (
    <View style={styles.header}>
      {leftshow &&
        <View style={{ width: SIZES.width * .2 }}>
          <Pressable onPress={onPressLeft} style={styles.left}>
            <AntDesign name='left' size={20} />
          </Pressable>
        </View>
      }
      <View style={{ width: SIZES.width * .6, ...STYLES }} >
        <Text style={styles.headerText}>{header}</Text>
      </View>
      <View style={{ width: SIZES.width * .2 }}>
        {
          source ?
            <Image style={{ width: '100%', height: '70%', ...imgstyle }} resizeMode='contain' source={source} />
            : rightshow &&
            <Pressable onPress={onPressRight} style={{ ...styles.left, alignSelf: 'flex-end' }}>
              <AntDesign name='right' size={20} />
            </Pressable>
        }
      </View>

    </View>
  )
}

export default CustomHeader

const styles = StyleSheet.create({
  header: {
    width: SIZES.width,
    height: 55,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    elevation: 2,
    backgroundColor: 'white'
  },
  left: {
    width: 30,
    height: 30,
    backgroundColor: 'white',
    // elevation: 5,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 20,
    marginVertical: 10,
    alignSelf: 'flex-start'
  }, headerText: {
    ...FONTS.h4,
    alignSelf: 'center'
  }
})