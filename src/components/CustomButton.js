import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import { COLORS, FONTS, STYLES } from '../constants/theme'

const CustomButton = ({ title, onPress, btnstyle, titleStyle, disabled }) => {
    return (
        <TouchableOpacity disabled={!disabled} onPress={onPress} style={{ ...styles.butnstyle, backgroundColor: !disabled ? 'lightgray' : 'black',...btnstyle }}>
            <Text style={{ ...styles.title, ...titleStyle }}>{title}</Text>
        </TouchableOpacity>
    )
}

export default CustomButton
const styles = StyleSheet.create({
    butnstyle: {
        backgroundColor: COLORS.black,
        width: '90%',
        ...STYLES,
        height: 50,
        borderRadius: 20
    }, title: {
        ...FONTS.h5,
        color: COLORS.white,
    }
})
