import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");

export const COLORS = {
    // base colors
    primary: '#FF6600',
    secondary: "",
    background: '#FAFAFA',
    // colors
    black: "#000000",
    white: "#FFFFFF",
    //others
    transparent: "transparent",
    darkgray: '#898C95',
};

export const STYLES = {
    justifyContent: 'center',
    alignItems: 'center'
}

export const SIZES = {
    // global sizes
    base: width * .03,
    base1: width * .02,
    font: 14,
    radius: 30,
    padding: width * .03,
    padding2: width * .05,

    // font sizes
    largeTitle: width * .14,
    mediumTitle: width * .1,
    h1: width * .08,
    h2: width * .065,
    h3: width * .055,
    h4: width * .045,
    h5: width * .035,
    // app dimensions
    width,
    height
};

export const FONTS = {
    h1: { fontFamily: "Quicksand", fontSize: SIZES.h1, color: COLORS.black, fontWeight: '600', lineHeight: width * .09 },
    h2: { fontFamily: "Quicksand", fontSize: SIZES.h2, color: COLORS.black, fontWeight: '600', lineHeight: width * .08 },
    h3: { fontFamily: "Quicksand", fontSize: SIZES.h3, color: COLORS.black, fontWeight: '600', lineHeight: width * .07 },
    h4: { fontFamily: "Quicksand", fontSize: SIZES.h4, color: COLORS.black, fontWeight: '600', lineHeight: width * .06 },
    h5: { fontFamily: "Quicksand", fontSize: SIZES.h5, color: COLORS.black, fontWeight: '600', lineHeight: width * .05 },
};
export const RFONTS = {
    h5: { fontFamily: "Quicksand", fontSize: width * .04, color: COLORS.black, fontWeight: '600' },
    h4: { fontFamily: "Quicksand", fontSize: width * .05, color: COLORS.black, fontWeight: '600' },
    h3: { fontFamily: "Quicksand", fontSize: width * .06, color: COLORS.black, fontWeight: '600' },
    h2: { fontFamily: "Quicksand", fontSize: width * .07, color: COLORS.black, fontWeight: '600' },
    h1: { fontFamily: "Quicksand", fontSize: width * .09, color: COLORS.black, fontWeight: '600' },
};
const appTheme = { COLORS, SIZES, FONTS, RFONTS };

export default appTheme;