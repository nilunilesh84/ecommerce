
export const NAVIGATION = {
    HOME: 'Home',
    REGISTER: 'Register',
    LOGIN: 'Login',
    CART: 'Cart',
    PRODUCTDETAIL: 'ProductDetail',
    PROFILE: 'Profile',
    MY_ACCOUNT: 'Myaccount',
    SETTINGS: 'Settings',
    SPLASH: 'SplashScreen',
    VERIFY: 'Verify',
    RESET: 'Reset',
    USER_QUES: 'UserQues',
    DRAWER: 'Drawer',
    HOME_N: 'HomeNavigator',
    AUTH: 'AuthNavigator',
    FAVORITE_TOPICS:'Favorite'
}